﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            webBrowser1.ScriptErrorsSuppressed = true;
            foreach (string path in Directory.GetFiles(@"./lists"))
            {
                File.AppendAllText("output.txt", path);
                foreach (string line in File.ReadAllLines(path))
                {
                    lockout = true;
                    try
                    {
                        webBrowser1.Url = new Uri(line);
                    }
                    catch (Exception ex)
                    {
                        Console.Write(ex);
                    }
                    webBrowser1.Update();
                    while (lockout)
                    {
                        Application.DoEvents();
                    }
                }
            }
        }

        bool lockout = false;

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            HtmlDocument htmlDoc = webBrowser1.Document;

            HtmlElement element;
            List<string> items = new List<string>();
            string msg = "";
            element = htmlDoc.GetElementById("PageHeader");
            foreach (HtmlElement child in element.Children)
            {
                if (child.OuterHtml.Contains("class=page-header__main"))
                {
                    foreach (HtmlElement subChild in child.Children)
                    {
                        if (subChild.OuterHtml.Contains("class=page-header__title"))
                        {
                            items.Add(subChild.InnerText);
                        }
                    }
                }
            }

            element = htmlDoc.GetElementById("mw-content-text");
            foreach (HtmlElement child in element.Children)
            {
                if (child.InnerText != null)
                {
                    if (!child.InnerText.Contains("Unearthed Arcana"))
                        items.Add(child.InnerText);
                }
            }
            string tempString = "";
            foreach (string s in items)
            {
                if (s != null)
                {
                    if (!s.Contains("http") && !s.Contains("This material"))
                    {

                        string temp = s.Replace(',', ' ');
                        temp = temp.Replace(':', ',');
                        temp = temp.Replace("\r\n", " ");
                        tempString += (temp + ',');
                    }
                }
            }
            tempString = RemoveLineEndings(tempString);
            tempString += Environment.NewLine;
            Console.Write(tempString);
            File.AppendAllText("output.txt", tempString);

            lockout = false;
        }
        public string RemoveLineEndings(string value)
        {
            if (String.IsNullOrEmpty(value))
            {
                return value;
            }
            string lineSeparator = ((char)0x2028).ToString();
            string paragraphSeparator = ((char)0x2029).ToString();

            return value.Replace("\r\n", string.Empty).Replace("\n", string.Empty).Replace("\r", string.Empty).Replace(lineSeparator, string.Empty).Replace(paragraphSeparator, string.Empty);
        }

    }
}

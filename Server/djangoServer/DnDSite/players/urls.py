from django.urls import path
from . import views


urlpatterns = [
    path('', views.displayCharacters),
    path('<int:idx>', views.viewCharacterSheet),
    path('<int:idx>/<str:item>/<str:mod>', views.modCharacterSheet),
]
from django.db import models
from django.contrib import admin

# Create your models here.
class player(models.Model):

    #Fields
    firstName = models.CharField(max_length=100)
    nickName = models.CharField(max_length=100,null=True,blank=True)
    lastName = models.CharField(max_length=100,null=True,blank=True)
    age = models.IntegerField(default=0)
    gender = models.CharField(default='male',max_length=100)
    alignment = models.CharField(max_length=100)
    weight = models.IntegerField(default=0)
    height = models.IntegerField(default=0)
    race = models.CharField(max_length=100)
    subrace = models.CharField(max_length=100,null=True,blank=True,default="")
    characterClass = models.CharField(max_length=100)
    subClass = models.CharField(max_length=100,null=True,blank=True,default="")
    level = models.IntegerField(null=True,blank=True,default=0)
    strength = models.IntegerField(null=True,blank=True,default=0)
    dexterity = models.IntegerField(null=True,blank=True,default=0)
    constitution = models.IntegerField(null=True,blank=True,default=0)
    charisma = models.IntegerField(null=True,blank=True,default=0)
    intelligence = models.IntegerField(null=True,blank=True,default=0)
    wisdom = models.IntegerField(null=True,blank=True,default=0)
    maxHealth = models.IntegerField(null=True,blank=True,default=0)
    currentHealth = models.IntegerField(null=True,blank=True,default=0)
    armourClass = models.IntegerField(null=True,blank=True,default=0)
    preparedSpells = models.TextField(blank=True,default="")
    spellSlots = models.TextField(blank=True,default="")
    weaponsEquipt = models.TextField(blank=True,default="")
    armourEquipt = models.TextField(blank=True,default="")
    inventory = models.TextField(blank=True,default="")
    gold = models.IntegerField(default=0)
    skills = models.TextField(blank=True,default="")

    class PlayerAdmin(admin.ModelAdmin):
        pass

    #MetaData
    class Meta:
        ordering = ["firstName"]


    #Methods
    def get_absolute_url(self):
        """
        Returns the url to access a particular instance of MyModelName.
        """
        return reverse('model-detail-view', args=[str(seld.id)])

    def __str__(self):
        """
        String for representing the MyModelName object (in Admin site etc.)
        """
        return self.firstName
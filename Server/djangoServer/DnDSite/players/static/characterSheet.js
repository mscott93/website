var CharacterInfo;
var ClassInfo;
var SubClassInfo;
var SpellListInfo;
var LevelTable;
var ArmourInfo;
var ItemInfo;
var WeaponInfo;

$(function (i) {
    $.ajaxSetup({
        async: false
    });
    collectData();
    console.log(CharacterInfo);
    console.log(ClassInfo);  
    console.log(CharacterInfo.armourEquipt);
    console.log(SubClassInfo);
    console.log(SpellListInfo);
    console.log(ItemInfo);
    console.log(ArmourInfo);
    console.log(WeaponInfo);
    displayTopBar();
    if(window.location.hash == "#CharacterInfo")
        displayCharacter();
    else if(window.location.hash == "#SpellInfo")
        displaySpells();
    else if(window.location.hash == "#ClassInfo")
        displayClass();
    else if(window.location.hash == "#EquipmentInfo")
        displayEquipment();
    else
        displayCharacter();
    $.ajaxSetup({
        async: true
    });
});

function collectData(){
    characterJson = characterJson.replace(new RegExp("&quot;", 'g'),"\"");
    characterJson = characterJson.replace(new RegExp("&#39;", 'g'),"\'");
    console.log(characterJson);
    CharacterInfo = JSON.parse(characterJson);
    CharacterInfo.spellSlots = $.parseJSON(CharacterInfo.spellSlots);
    CharacterInfo.preparedSpells = $.parseJSON(CharacterInfo.preparedSpells);
    CharacterInfo.weaponsEquipt = $.parseJSON(CharacterInfo.weaponsEquipt);
    CharacterInfo.armourEquipt = $.parseJSON(CharacterInfo.armourEquipt);
    CharacterInfo.inventory = $.parseJSON(CharacterInfo.inventory);
    $.getJSON("/static/spells.json", function(data){
        SpellListInfo = data;
    });  
     $.getJSON("/static/items.json", function(data){
        ItemInfo = data;
    }); 
    $.getJSON("/static/armour.json", function(data){
        ArmourInfo = data;
    });   
    $.getJSON("/static/weapons.json", function(data){
        WeaponInfo = data;
    });     
    $.getJSON("/static/Classes.json", function(data){
        ClassInfo = data[CharacterInfo.characterClass];   
        $(ClassInfo.subclass).each(function(idx,obj){
            if(obj.name == CharacterInfo.characterSubClass)
                SubClassInfo = obj;
        })
        LevelTable = ClassInfo.levelTable;
        if(SubClassInfo){
            if(SubClassInfo.levelTable)
                LevelTable = SubClassInfo.levelTable;
        }
    });  
}

function getUrlVars() {
    var vars = [],
        hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function displayTopBar(){
    /// Name etc ///
    document.getElementById("characterName").innerHTML = CharacterInfo.firstName;
    if(CharacterInfo.nickName)
        document.getElementById("characterName").innerHTML += " '<i>" + CharacterInfo.nickName + "</i>'";
    if(CharacterInfo.lastName)
        document.getElementById("characterName").innerHTML += " " + CharacterInfo.lastName;

    document.getElementById("characterDefinition").innerHTML = "";
    if(CharacterInfo.subrace)
        document.getElementById("characterDefinition").innerHTML += CharacterInfo.subrace + " ";
    if(CharacterInfo.race)
        document.getElementById("characterDefinition").innerHTML += CharacterInfo.race + " ";
    if(SubClassInfo)
        document.getElementById("characterDefinition").innerHTML += SubClassInfo.name + " ";
    if(ClassInfo.name)
        document.getElementById("characterDefinition").innerHTML += ClassInfo.name + " ";
}

function displayCharacter() {
    hideAllInfo();
    window.location.hash = "CharacterInfo";
    document.getElementById("displayCharacterTab").classList.add("is-active");

    /// HP Bar ///
    document.getElementById("characterHPText").innerHTML = "HP : " + CharacterInfo.currentHealth + "/" + CharacterInfo.maxHealth;
    var temphp = CharacterInfo.currentHealth;

    var slider = document.getElementById("myRange");
    slider.value = (temphp / CharacterInfo.maxHealth) * 100;

    //setup before functions
    var typingTimer;                //timer identifier
    var doneTypingInterval = 1000;  //time in ms, 5 second for example

    slider.oninput = function(){
        temphp = Math.round((this.value/100) * CharacterInfo.maxHealth);
        document.getElementById("characterHPText").innerHTML = "HP : " + temphp + "/" + CharacterInfo.maxHealth;
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    }

    //user is "finished typing," do something
    function doneTyping () {
        $.get(window.location.href.split('#')[0] + '/currentHealth/' + temphp);
    }

    /// setup table ///
    var ctx = $("#classChart");
    Chart.defaults.global.tooltips.titleFontSize = 24;
    Chart.defaults.global.tooltips.bodyFontSize = 18;
    Chart.defaults.global.tooltips.bodyFontFamily = "Scaly Sans Caps";
    Chart.defaults.global.tooltips.displayColors = false;
    Chart.defaults.global.defaultFontFamily = "Nodesto";
    Chart.defaults.global.elements.point.radius = 7;
    Chart.defaults.global.elements.point.hitRadius = 80;
    Chart.defaults.global.legend.display = false;
    var myChart = new Chart(ctx, {
        type: 'radar',
        data: {           
            labels: ["Str", "Dex", "Con", "Wis", "Int", "Cha"],
            datasets: [{
                label: 'Value',
                data: [CharacterInfo.strength,
                       CharacterInfo.dexterity,
                       CharacterInfo.constitution,
                       CharacterInfo.wisdom,
                       CharacterInfo.intelligence,
                       CharacterInfo.charisma
                      ],
                backgroundColor: "rgba(0,0,0,0.5)"
            }]
        },
        options: {
            tooltips: {
                enabled: true,
                mode: 'single',
                callbacks: {
                    afterLabel: function(tooltipItems, data) { 
                        return "Modifier: " + String(Math.floor(tooltipItems.yLabel / 2) -5);
                    }
                }
            },
            scale: {
                pointLabels: {
                        fontSize: 32                   
                },
                ticks: {
                    fontSize: 18,
                    max: 20,
                    min: 0,
                    beginAtZero: true
                }
            }
        }
    });

    /// Skill Table
    var table = document.getElementById("skillTable")

    for(i = 0; i < 18; i++){
        var attribute;
            attribute = CharacterInfo.strength;
        if(i > 0)
            attribute = CharacterInfo.dexterity;
        if(i > 3)
            attribute = CharacterInfo.intelligence;
        if(i > 8)
            attribute = CharacterInfo.wisdom;
        if(i > 13)
            attribute = CharacterInfo.charisma;

        if(CharacterInfo.skills[i]){
            var row = table.getElementsByTagName("tr")[i+1];
            var mod = Math.floor(attribute/2) - 5;
            row.getElementsByTagName("td")[4].innerHTML =  (Math.sign(mod) != -1? "+" : "") + mod;
            if(CharacterInfo.skills[i].isProficient == true){
                row.getElementsByTagName("td")[2].innerHTML = '<i class="fas fa-check"></i>'
                mod = Math.floor(attribute/2) - 5 + Math.ceil(CharacterInfo.level/4 +1);
                row.getElementsByTagName("td")[4].innerHTML = (Math.sign(mod) != -1? "+" : "-") + mod;
            }
            if(CharacterInfo.skills[i].isExpert == true){
                row.getElementsByTagName("td")[3].innerHTML = '<i class="fas fa-check"></i>'
                mod =Math.floor(attribute/2) - 5 + (Math.ceil(CharacterInfo.level/4 +1)*2);
                row.getElementsByTagName("td")[4].innerHTML = (Math.sign(mod) != -1? "+" : "-") + mod;
            }           
        }
    }

    $("#CharacterInfo").show();
}

function displaySpells() {
    hideAllInfo();
    window.location.hash = "SpellInfo"
    document.getElementById("displaySpellsTab").classList.add("is-active");

    //Populate SpellSlot Table
    $("#spellSlotDisplay").empty();
    var tableRow = document.createElement("tr");
    tableRow.classList.add("tr")
    document.getElementById("spellSlotDisplay").appendChild(tableRow);
    for(i = 0; i < 9; i++) {
        var obj= document.createElement("th");
        obj.classList.add("th");
        obj.innerHTML = (i + 1) + "th" 
        if((i + 1) == 1)
            obj.innerHTML = (i + 1) + "st" 
        if((i + 1) == 2)
            obj.innerHTML = (i + 1) + "nd" 
        if((i + 1) == 3)
            obj.innerHTML = (i + 1) + "rd" 
        tableRow.appendChild(obj);
    }
    tableRow = document.createElement("tr");
    tableRow.classList.add("tr")
    document.getElementById("spellSlotDisplay").appendChild(tableRow);
    for(i = 0; i < 9; i++) {
        var obj= document.createElement("td");
        obj.setAttribute("onclick","incrementSpellSlot("+i+")");
        obj.innerHTML = '<i class="fas fa-caret-up"></i>';
        tableRow.appendChild(obj);
    };
    tableRow = document.createElement("tr");
    tableRow.classList.add("tr")
    document.getElementById("spellSlotDisplay").appendChild(tableRow);
    $(CharacterInfo.spellSlots).each(function (i, entry) {
        var obj= document.createElement("td");
        obj.classList.add("td");
        obj.innerHTML = entry
        tableRow.appendChild(obj);
    });
        tableRow = document.createElement("tr");
    tableRow.classList.add("tr")
    document.getElementById("spellSlotDisplay").appendChild(tableRow);
    for(i = 0; i < 9; i++) {
        var obj= document.createElement("td");
        obj.setAttribute("onclick", "decrementSpellSlot("+ i+")");
        obj.innerHTML = '<i class="fas fa-caret-down"></i>';
        tableRow.appendChild(obj);
    };

    //Get Spells this character can use
    var spellIndexs = [];
    $(CharacterInfo.preparedSpells).each(function(i,obj){
            spellIndexs.push(obj);
        });
    if(SubClassInfo){
        $(SubClassInfo.spells).each(function(i,obj){
            if(obj.levelAcquired <= CharacterInfo.level)
                $(obj.idList).each(function(i,k){
                    spellIndexs.push(k);
                });
        });
    }

    //populate current spells spellbook
    $("#spellListHolder").empty();
    for(j = 0; j < 9; j++){
        $(spellIndexs).each(function(i,obj){
            var spell = SpellListInfo[obj]
            if(spell.spellSlot == j){
                var listEntry = document.createElement("box");
                listEntry.classList.add("box");
                var hero = document.createElement("div");
                hero.classList.add("hero");
                hero.classList.add("is-light");
                listEntry.appendChild(hero);
                var heroHead = document.createElement("div");
                heroHead.classList.add("hero-head");
                hero.appendChild(heroHead);
                var heading = document.createElement("h4");
                heading.innerHTML = spell.name;
                heading.classList.add("title");
                heroHead.appendChild(heading); 
                hero.setAttribute("onclick", "showSpell(" + obj +")");
                var summary = document.createElement("p");
                if(spell.spellSlot == 0)
                    summary.innerHTML = "School: "+ spell.school + "<br/>Cantrip";
                else
                    summary.innerHTML = "School: "+ spell.school + "<br/>Spell Slot: Level " + spell.spellSlot;
                listEntry.appendChild(summary);
                document.getElementById("spellListHolder").appendChild(listEntry);
            }
        })
    }
    
    $("#SpellInfo").show();
}

function displayClass() {
    hideAllInfo();
    window.location.hash = "ClassInfo"
    document.getElementById("displayClassTab").classList.add("is-active");

    if (SubClassInfo)
        document.getElementById("classTitle").innerHTML = SubClassInfo.name + " " + ClassInfo.name;
    else
        document.getElementById("classTitle").innerHTML = ClassInfo.name;
    document.getElementById("classSummary").innerHTML = ClassInfo.summary;
    if(SubClassInfo)
        document.getElementById("classSummary").innerHTML += "<br/><br/>" + SubClassInfo.description;

    $("#levelTable").empty();
    $(LevelTable).each(function (i, row) {
        var tableRow = document.createElement("tr");
        tableRow.classList.add("tr")
        if (CharacterInfo.level == i)
            tableRow.classList.add("is-selected");
        document.getElementById("levelTable").appendChild(tableRow);
        $(row).each(function (l, column) {
            var entry;
            if (i == 0) {
                entry = document.createElement("th");
                entry.classList.add("th");
            } else {
                entry = document.createElement("td");
                entry.classList.add("td");
            }
            entry.innerHTML = column;
            tableRow.appendChild(entry);
        });
    });

    $("#classAbilityList").empty();
    $(ClassInfo.abilities).each(function (i, obj) {
        var listEntry = document.createElement("box");
        listEntry.classList.add("box");
        var hero = document.createElement("div");
        hero.classList.add("hero");
        hero.classList.add("is-light");
        listEntry.appendChild(hero);
        var heroHead = document.createElement("div");
        heroHead.classList.add("hero-head");
        hero.appendChild(heroHead);
        var heading = document.createElement("h4");
        heading.innerHTML = obj.name;
        heading.classList.add("title");
        heroHead.appendChild(heading);
        if (CharacterInfo.level >= obj.levelAcquired) {
            hero.setAttribute("onclick", "showSkill(" + i + "," + false + ")");
        } else {
            var levelRequirement = document.createElement("h4");
            levelRequirement.innerHTML = "<i>Unlock at level " + obj.levelAcquired + "</i>";
            levelRequirement.classList.add("subtitle");
            heroHead.appendChild(levelRequirement);
        }
        var summary = document.createElement("p");
        summary.innerHTML = obj.summary;
        listEntry.appendChild(summary);
        document.getElementById("classAbilityList").appendChild(listEntry);
    });
    console.log(CharacterInfo.characterSubClass);
    if (SubClassInfo){
        $(SubClassInfo.abilities).each(function (i, obj) {
        var listEntry = document.createElement("box");
        listEntry.classList.add("box");
        var hero = document.createElement("div");
        hero.classList.add("hero");
        hero.classList.add("is-light");
        listEntry.appendChild(hero);
        var heroHead = document.createElement("div");
        heroHead.classList.add("hero-head");
        hero.appendChild(heroHead);
        var heading = document.createElement("h4");
        heading.innerHTML = obj.name;
        heading.classList.add("title");
        heroHead.appendChild(heading);
        if (CharacterInfo.level >= obj.levelAcquired) {
            hero.setAttribute("onclick", "showSkill(" + i + "," + true + ")");
        } else {
            var levelRequirement = document.createElement("h4");
            levelRequirement.innerHTML = "<i>Unlock at level " + obj.levelAcquired + "</i>";
            levelRequirement.classList.add("subtitle");
            heroHead.appendChild(levelRequirement);
        }
        var summary = document.createElement("p");
        summary.innerHTML = obj.summary;
        listEntry.appendChild(summary);
        document.getElementById("classAbilityList").appendChild(listEntry);
    });
    }
    /*
    if ((CharacterInfo.subclass == null) && CharacterInfo.level >= ClassInfo.subclassLevel) {
        showSubclassSelector();
    }
    */
    $("#ClassInfo").show();
}

function displayEquipment() {
    hideAllInfo();
    window.location.hash = "EquipmentInfo"
    document.getElementById("displayEquipmentTab").classList.add("is-active");
    if(CharacterInfo.gender == "male")
        document.getElementById("characterImage").setAttribute("src","/static/Content/characterMale.png");
    else
        document.getElementById("characterImage").setAttribute("src","/static/Content/characterFemale.png");
        
    document.getElementById("playerGold").value = CharacterInfo.gold;

    var uiElement;
    uiElement = document.getElementsByClassName("box armour")[0];
    if(CharacterInfo.armourEquipt[0].length != 0)
        uiElement.onclick = function(){showArmour(CharacterInfo.armourEquipt[0]);}
    else
        uiElement.style.display = "none";
    uiElement = document.getElementsByClassName("box helmet")[0];
    if(CharacterInfo.armourEquipt[1].length != 0)
        uiElement.onclick = function(){showArmour(CharacterInfo.armourEquipt[1]);}
    else
        uiElement.style.display = "none";
    uiElement = document.getElementsByClassName("box ring")[0];
    if(CharacterInfo.armourEquipt[2].length != 0)
        uiElement.onclick = function(){showItem(CharacterInfo.armourEquipt[2]);}
    else
        uiElement.style.display = "none";
    uiElement = document.getElementsByClassName("box amulet")[0];
    if(CharacterInfo.armourEquipt[3].length != 0)
        uiElement.onclick = function(){showArmour(CharacterInfo.armourEquipt[3]);}
    else
        uiElement.style.display = "none";
    uiElement = document.getElementsByClassName("box boots")[0];
    if(CharacterInfo.armourEquipt[4].length != 0)
        uiElement.onclick = function(){showArmour(CharacterInfo.armourEquipt[4]);}
    else
        uiElement.style.display = "none";
    uiElement = document.getElementsByClassName("box shield")[0];
    if(CharacterInfo.armourEquipt[5].length != 0)
        uiElement.onclick = function(){showArmour(CharacterInfo.armourEquipt[5]);}
    else
        uiElement.style.display = "none";
    uiElement = document.getElementsByClassName("box trinkets")[0];
    if(CharacterInfo.armourEquipt[6].length != 0)
        uiElement.onclick = function(){showItem(CharacterInfo.armourEquipt[6]);}
    else
        uiElement.style.display = "none";

    uiElement = document.getElementsByClassName("box weapon")[0];
    if(CharacterInfo.weaponsEquipt.length != 0)
        uiElement.onclick = function(){showWeapon(CharacterInfo.weaponsEquipt);}
    else
        uiElement.style.display = "none";

    $("#characterInventoryList").empty();
    $(CharacterInfo.inventory).each(function (i, obj) {
        var listEntry = document.createElement("box");
        listEntry.classList.add("box");
        var hero = document.createElement("div");
        hero.classList.add("hero");
        hero.classList.add("is-light");
        listEntry.appendChild(hero);
        var heroHead = document.createElement("div");
        heroHead.classList.add("hero-head");
        hero.appendChild(heroHead);
        var heading = document.createElement("h4");
        heading.innerHTML = ItemInfo[obj].Name;
        heading.classList.add("title");
        heroHead.appendChild(heading);
        listEntry.onclick = function(){showItem([obj]);}
        document.getElementById("characterInventoryList").appendChild(listEntry);
    });

    $("#EquipmentInfo").show();
}

function resetSpellSlots(){
    
    for(j = 0; j < 9; j++){
        for(i = 0; i < LevelTable[0].length; i++){
            if(~LevelTable[0][i].indexOf('Slots ' + String(j+1))){
                spellSlotLocation = i;
                CharacterInfo.spellSlots[j] = LevelTable[CharacterInfo.level][i];
            }
        }
    }     
    //collectData();
    displaySpells();
    $.get(window.location.href.split('#')[0] + '/spellSlots/' + JSON.stringify(CharacterInfo.spellSlots));
}

function saveGoldValue(){
    $.get(window.location.href.split('#')[0] + '/gold/' + document.getElementById("playerGold").value);
}

function incrementSpellSlot(value){  
    var maxSpellSlotValue;
    for(i = 0; i < LevelTable[0].length; i++){
        if(~LevelTable[0][i].indexOf('Slots ' + String(value+1))){
            maxSpellSlotValue = LevelTable[CharacterInfo.level][i];
        }
    }
    if(CharacterInfo.spellSlots[value] < maxSpellSlotValue)
        CharacterInfo.spellSlots[value]++;
    displaySpells();
    $.get(window.location.href.split('#')[0] + '/spellSlots/' + JSON.stringify(CharacterInfo.spellSlots));
}

function decrementSpellSlot(value){
    if(CharacterInfo.spellSlots[value] > 0)
        CharacterInfo.spellSlots[value]--;
    displaySpells();
    $.get(window.location.href.split('#')[0] + '/spellSlots/' + JSON.stringify(CharacterInfo.spellSlots));
}

function hideAllInfo() {
    $("#mainContent").children('div').each(function (i, obj) {
        $(obj).hide();
    });
    $("#navigationTab").children('li').each(function (i, obj) {
        obj.classList.remove("is-active");
    });
}

function showModal(value) {
    if (value)
        document.getElementById("pageNotification").classList.add("is-active");
    else
        document.getElementById("pageNotification").classList.remove("is-active");
}

function showSubclassSelector() {
    showModal(true);
}

function showSkill(index, isSubclass) {
    showModal(true);
    if (isSubclass) {
        var content = document.getElementById("pageNotificationContent");
        $(content).empty();

        var obj = SubClassInfo.abilities[index];

        var listEntry = document.createElement("box");
        listEntry.classList.add("box");
        var hero = document.createElement("div");
        hero.classList.add("hero");
        hero.classList.add("is-dark");
        listEntry.appendChild(hero);
        var heroHead = document.createElement("div");
        heroHead.classList.add("hero-head");
        hero.appendChild(heroHead);
        var heading = document.createElement("h4");
        heading.innerHTML = obj.name;
        heading.classList.add("title");
        heroHead.appendChild(heading);
        var summary = document.createElement("p");
        summary.innerHTML = obj.summary + "<br/>" + obj.detail;
        listEntry.appendChild(summary);

        content.appendChild(listEntry);
    } else {
        var content = document.getElementById("pageNotificationContent");
        $(content).empty();

        var obj = ClassInfo.abilities[index];

        var listEntry = document.createElement("box");
        listEntry.classList.add("box");
        var hero = document.createElement("div");
        hero.classList.add("hero");
        hero.classList.add("is-dark");
        listEntry.appendChild(hero);
        var heroHead = document.createElement("div");
        heroHead.classList.add("hero-head");
        hero.appendChild(heroHead);
        var heading = document.createElement("h4");
        heading.innerHTML = obj.name;
        heading.classList.add("title");
        heroHead.appendChild(heading);
        var summary = document.createElement("p");
        summary.innerHTML = obj.summary + "<br/>" + obj.detail;
        listEntry.appendChild(summary);

        content.appendChild(listEntry);
    }
}

function showArmour(index) {
    showModal(true);
    var content = document.getElementById("pageNotificationContent");
    $(content).empty();

    $(index).each(function(i,entry){
        var obj = ArmourInfo[entry];

        var listEntry = document.createElement("box");
        listEntry.classList.add("box");
        var hero = document.createElement("div");
        hero.classList.add("hero");
        hero.classList.add("is-dark");
        listEntry.appendChild(hero);
        var heroHead = document.createElement("div");
        heroHead.classList.add("hero-head");
        hero.appendChild(heroHead);
        var heading = document.createElement("h4");
        heading.innerHTML = obj.Name;
        heading.classList.add("title");
        heroHead.appendChild(heading);
        var summary = document.createElement("p");
        summary.innerHTML += "Armour Class: " + obj["Armor Class"] + "<br/>";
        summary.innerHTML += "Stealth: " + obj["Stealth"] + "<br/>";
        summary.innerHTML += "Strength Requirement: " + obj["Strength"] + "<br/>";
        summary.innerHTML += "Value: " + obj["Value"] + "<br/>";
        summary.innerHTML += "Weight: " + obj["Weight"] + "<br/>";
        summary.innerHTML += "Rules: " + obj["Rules"] + "<br/>";
        listEntry.appendChild(summary);

        content.appendChild(listEntry);
    })
}

function showWeapon(index) {
    showModal(true);
    var content = document.getElementById("pageNotificationContent");
    $(content).empty();

    $(index).each(function(i,entry){
        var obj = WeaponInfo[entry];

        var listEntry = document.createElement("box");
        listEntry.classList.add("box");
        var hero = document.createElement("div");
        hero.classList.add("hero");
        hero.classList.add("is-dark");
        listEntry.appendChild(hero);
        var heroHead = document.createElement("div");
        heroHead.classList.add("hero-head");
        hero.appendChild(heroHead);
        var heading = document.createElement("h4");
        heading.innerHTML = obj.Name;
        heading.classList.add("title");
        heroHead.appendChild(heading);
        var summary = document.createElement("p");
        summary.innerHTML += "Damage: " + obj["Damage"] + "<br/>";
        summary.innerHTML += "Range: " + obj["Range"] + "<br/>";
        summary.innerHTML += "Value: " + obj["Value"] + "<br/>";
        summary.innerHTML += "Weight: " + obj["Weight"] + "<br/>";
        if(obj["isMartial"] == "TRUE")
            summary.innerHTML += "Martial Weapon: " + '<i class="fas fa-check"></i>' + "<br/>";
        else
            summary.innerHTML += "Martial Weapon: " + '<i class="fas fa-times"></i>' + "<br/>";
        summary.innerHTML += "Rules: " + obj["Rules"] + "<br/>";
        listEntry.appendChild(summary);

        content.appendChild(listEntry);
    })
}

function showItem(index) {
    showModal(true);
    var content = document.getElementById("pageNotificationContent");
    $(content).empty();
    $(index).each(function(i,entry){
        var obj = ItemInfo[entry];
        var listEntry = document.createElement("box");
        listEntry.classList.add("box");
        var hero = document.createElement("div");
        hero.classList.add("hero");
        hero.classList.add("is-dark");
        listEntry.appendChild(hero);
        var heroHead = document.createElement("div");
        heroHead.classList.add("hero-head");
        hero.appendChild(heroHead);
        var heading = document.createElement("h4");
        heading.innerHTML = obj.Name;
        heading.classList.add("title");
        heroHead.appendChild(heading);
        var summary = document.createElement("p");
        summary.innerHTML += "Value: " + obj["Cost"] + "<br/>";
        summary.innerHTML += "Weight: " + obj["Weight"] + "<br/>";
        summary.innerHTML += "Rules: " + obj["Rules"] + "<br/>";
        listEntry.appendChild(summary);

        content.appendChild(listEntry);
    })
}

function showSpell(index) {
    showModal(true);

    var obj = SpellListInfo[index];
    var content = document.getElementById("pageNotificationContent");
    $(content).empty();

    var listEntry = document.createElement("box");
    listEntry.classList.add("box");
    var hero = document.createElement("div");
    hero.classList.add("hero");
    hero.classList.add("is-dark");
    listEntry.appendChild(hero);
    var heroHead = document.createElement("div");
    heroHead.classList.add("hero-head");
    hero.appendChild(heroHead);
    var heading = document.createElement("h4");
    heading.innerHTML = obj.name;
    heading.classList.add("title");
    heroHead.appendChild(heading);
    var summary = document.createElement("p");
    summary.innerHTML = "";
    if(obj.spellSlot == 0)
        summary.innerHTML += "Spell Slot: Cantrip<br/>";
    else
        summary.innerHTML += "Spell Slot: " + obj.spellSlot + "<br/>";
    summary.innerHTML += "School: " + obj.school + "<br/>";
    summary.innerHTML += "Range: " + obj.range + "<br/>";
    summary.innerHTML += "Components: " + obj.components + "<br/>";
    summary.innerHTML += "Casting Time: " + obj.castingTime + "<br/>";
    summary.innerHTML += "Duration: " + obj.duration + "<br/><br/>";
    summary.innerHTML += obj.detail;
    listEntry.appendChild(summary);

    content.appendChild(listEntry);
}

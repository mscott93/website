from django.shortcuts import render
from django.http import HttpResponse
from .models import player
import re

def displayCharacters(request):
    html = ''
    for p in player.objects.all():
        html += '<a href= ' + request.get_full_path() + str(p.id) + '>' + p.firstName + '</a><br/>'
    return HttpResponse(html)

# Create your views here.
def viewCharacterSheet(request, idx):
    entry = player.objects.get(pk=idx)
    html = '{'
    html += '"firstName": "' + entry.firstName + '",'
    if (entry.nickName):
        html += '"nickName": "' + entry.nickName + '",'
    if (entry.lastName):
        html += '"lastName": "' + entry.lastName + '",'
    html += '"race": "' + entry.race + '",'
    if (entry.subrace):
        html += '"subrace": "' + entry.subrace + '",'
    html += '"age": "' + str(entry.age) + '",'
    html += '"alignment": "' + entry.alignment + '",'
    html += '"characterClass": "' + str(entry.characterClass) + '",'
    html += '"characterSubClass": "' + str(entry.subClass) + '",'
    html += '"gender": "' + entry.gender + '",'
    html += '"weight": "' + str(entry.weight) + '",'
    html += '"height": "' + str(entry.height) + '",'
    html += '"level": "' + str(entry.level) + '",'
    html += '"strength": "' + str(entry.strength) + '",'
    html += '"dexterity": "' + str(entry.dexterity) + '",'
    html += '"constitution": "' + str(entry.constitution) + '",'    
    html += '"charisma": "' + str(entry.charisma) + '",'
    html += '"intelligence": "' + str(entry.intelligence) + '",'
    html += '"wisdom": "' + str(entry.wisdom) + '",'
    html += '"maxHealth": "' + str(entry.maxHealth) + '",'
    html += '"currentHealth": "' + str(entry.currentHealth) + '",'
    html += '"gold": "' + str(entry.gold) + '",'
    html += '"preparedSpells": "' + str(entry.preparedSpells) + '",' 
    html += '"spellSlots": "' + str(entry.spellSlots) + '",' 
    html += '"weaponsEquipt": "' + str(entry.weaponsEquipt) + '",' 
    html += '"armourEquipt": "' + str(entry.armourEquipt) + '",' 
    html += '"skills": ' + str(entry.skills) + ','
    html += '"inventory": "' + str(entry.inventory) + '"' 
    html += '}'

    html = re.escape(html)

    return render(
        request,
        'Website/characterSheet.html',
        context={'json': html}
        )

    return HttpResponse(html)

def modCharacterSheet(request, idx, item, mod):
    entry = player.objects.get(pk=idx)
    if(item == "currentHealth"):
        if(int(mod) <= entry.maxHealth):
            player.objects.filter(id = idx).update(currentHealth = mod)
            return HttpResponse("Updated " + str(item) + " " + str(entry.currentHealth))
        else:
            return HttpResponse("Invalid Request")

    if(item == "maxHealth"):
        player.objects.filter(id = idx).update(maxHealth = mod)
        return HttpResponse("Updated " + str(item) + " " + str(entry.maxHealth))

    if(item == "spellSlots"):
        player.objects.filter(id = idx).update(spellSlots = mod)
        return HttpResponse("Updated " + str(item) + " " + str(entry.spellSlots))

    if(item == "gold"):
        player.objects.filter(id = idx).update(gold = mod)
        return HttpResponse("Updated " + str(item) + " " + str(entry.gold))

    return HttpResponse("Invalid Request")
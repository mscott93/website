# Generated by Django 2.0.2 on 2018-03-14 09:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0002_auto_20180207_0746'),
    ]

    operations = [
        migrations.AddField(
            model_name='player',
            name='armourEquipt',
            field=models.TextField(default=''),
        ),
        migrations.AddField(
            model_name='player',
            name='inventory',
            field=models.TextField(default=''),
        ),
        migrations.AddField(
            model_name='player',
            name='preparedSpells',
            field=models.TextField(default=''),
        ),
        migrations.AddField(
            model_name='player',
            name='spellSlots',
            field=models.TextField(default=''),
        ),
        migrations.AddField(
            model_name='player',
            name='weaponsEquipt',
            field=models.TextField(default=''),
        ),
    ]

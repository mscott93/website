# Generated by Django 2.0.2 on 2018-03-14 10:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0003_auto_20180314_0920'),
    ]

    operations = [
        migrations.AlterField(
            model_name='player',
            name='armourEquipt',
            field=models.TextField(blank=True, default=''),
        ),
        migrations.AlterField(
            model_name='player',
            name='inventory',
            field=models.TextField(blank=True, default=''),
        ),
        migrations.AlterField(
            model_name='player',
            name='preparedSpells',
            field=models.TextField(blank=True, default=''),
        ),
        migrations.AlterField(
            model_name='player',
            name='spellSlots',
            field=models.TextField(blank=True, default=''),
        ),
        migrations.AlterField(
            model_name='player',
            name='weaponsEquipt',
            field=models.TextField(blank=True, default=''),
        ),
    ]

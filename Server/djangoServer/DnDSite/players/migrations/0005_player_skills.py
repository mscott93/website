# Generated by Django 2.0.2 on 2018-03-14 16:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0004_auto_20180314_1046'),
    ]

    operations = [
        migrations.AddField(
            model_name='player',
            name='skills',
            field=models.TextField(blank=True, default=''),
        ),
    ]

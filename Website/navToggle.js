$(function(){
    $("#nav").load("navHTML.html", function(){
        var playerId = getUrlVars()["id"]
        var f = $.getJSON('playerCharacters.json', function(data)
            {
                $('#characterLink').each(function(i,obj){obj.href = "character.html?id=" + playerId;});
                $('#skillLink').each(function(i,obj){obj.href = "classProfile.html?id=" + playerId;});
                $('#spellbookLink').each(function(i,obj){;});
                $('#equipmentLink').each(function(i,obj){;});
            });   
    }); 
});

/* Set the width of the side navigation to 250px and the left margin of the page content to 250px */
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
}

/* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
}


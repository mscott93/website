// Read a page's GET URL variables and return them as an associative array.
function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
var CharacterInfo;
var ClassInfo;
var SubClassInfo;
$(function(){     
    $.getJSON('playerCharacters.json', function(data)
    {
        CharacterInfo = data.character[getUrlVars()["id"]];        
    });
    $.getJSON('Classes.json', function(data)
    {
        ClassInfo = data[CharacterInfo["class"]];
        console.log(ClassInfo);
        SubClassInfo = ClassInfo.subclass[CharacterInfo.subclass];
    });
});
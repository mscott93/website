var CharacterInfo;
var ClassInfo;
var SubClassInfo;
var SpellListInfo;

$(function (i) {
    $.ajaxSetup({
        async: false
    });
    console.log(location.port);
    $.getJSON('playerCharacters.json', function (data) {
        CharacterInfo = data.character[getUrlVars()["id"]];
        console.log(CharacterInfo);
    });
    $.getJSON('Classes.json', function (data) {
        ClassInfo = data[CharacterInfo["class"]];
        console.log(ClassInfo);
        SubClassInfo = ClassInfo.subclass[CharacterInfo.subclass];
    });
    $.getJSON('spells.json', function (data) {
        SpellListInfo = data;
        console.log(SpellListInfo);
    });
    displayCharacter();
    $.ajaxSetup({
        async: true
    });
});

function getUrlVars() {
    var vars = [],
        hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function displayCharacter() {
    hideAllInfo();
    document.getElementById("displayCharacterTab").classList.add("is-active");
    if (CharacterInfo.nickName != "") {
        document.getElementById("characterName").innerHTML = CharacterInfo.firstName + " '<i>" + CharacterInfo.nickName + "</i> ' " + CharacterInfo.lastName;
    } else {
        document.getElementById("characterName").innerHTML = CharacterInfo.firstName + " " + CharacterInfo.lastName;
    }
    if (CharacterInfo.subclass != null) {
        document.getElementById("characterDefinition").innerHTML = CharacterInfo.subrace + " " + CharacterInfo.race + " " + SubClassInfo.name + " " + CharacterInfo.class;
    } else {
        document.getElementById("characterDefinition").innerHTML = CharacterInfo.subrace + " " + CharacterInfo.race + " " + CharacterInfo.class;
    }

    document.getElementById("characterHPText").innerHTML = "HP : " + CharacterInfo.currentHealth + "/" + CharacterInfo.maxHealth;
    document.getElementById("characterHPBar").value = CharacterInfo.currentHealth;
    document.getElementById("characterHPBar").max = CharacterInfo.maxHealth;

    var ctx = $("#classChart");
    Chart.defaults.global.defaultFontSize = 14;
    Chart.defaults.global.defaultFontFamily = "Nodesto";
    Chart.defaults.global.elements.point.radius = 7;
    Chart.defaults.global.elements.point.hitRadius = 40;
    Chart.defaults.global.legend.display = false;
    var myChart = new Chart(ctx, {
        type: 'radar',
        data: {
            labels: ["Strength", "Dexterity", "Constitution", "Wisdom", "Intelligence", "Charisma"],
            datasets: [{
                label: 'Value',
                data: [CharacterInfo.strength,
                       CharacterInfo.dexterity,
                       CharacterInfo.constitution,
                       CharacterInfo.wisdom,
                       CharacterInfo.intelligence,
                       CharacterInfo.charisma
                      ],
                backgroundColor: "rgba(0,0,0,0.5)"
            }]
        },
        options: {
            scale: {
                ticks: {
                    max: 20,
                    min: 0,
                    beginAtZero: true
                }
            }
        }
    });

    $("#CharacterInfo").show();
}

function displaySpells() {
    hideAllInfo();
    document.getElementById("displaySpellsTab").classList.add("is-active");
    
    $(ClassInfo.spells).each(function(i,obj){
        var spellName = document.createElement("div");
        spellName.innerHTML = SpellListInfo[obj].name;
        document.getElementById("spellListHolder").appendChild(spellName);
    })
    
    $("#SpellInfo").show();
}

function displayClass() {
    hideAllInfo();
    document.getElementById("displayClassTab").classList.add("is-active");

    if (CharacterInfo.subclass != null)
        document.getElementById("classTitle").innerHTML = SubClassInfo.name + " " + ClassInfo.name;
    else
        document.getElementById("classTitle").innerHTML = ClassInfo.name;
    document.getElementById("classSummary").innerHTML = ClassInfo.summary;

    $("#levelTable").empty();
    $(ClassInfo.levelTable).each(function (i, row) {
        var tableRow = document.createElement("tr");
        tableRow.classList.add("tr")
        if (CharacterInfo.level == i)
            tableRow.classList.add("is-selected");
        document.getElementById("levelTable").appendChild(tableRow);
        $(row).each(function (l, column) {
            var entry;
            if (i == 0) {
                entry = document.createElement("th");
                entry.classList.add("th");
            } else {
                entry = document.createElement("td");
                entry.classList.add("td");
            }
            entry.innerHTML = column;
            tableRow.appendChild(entry);
        });
    });

    $("#classAbilityList").empty();
    $(ClassInfo.abilities).each(function (i, obj) {
        var listEntry = document.createElement("box");
        listEntry.classList.add("box");
        var hero = document.createElement("div");
        hero.classList.add("hero");
        hero.classList.add("is-light");
        listEntry.appendChild(hero);
        var heroHead = document.createElement("div");
        heroHead.classList.add("hero-head");
        hero.appendChild(heroHead);
        var heading = document.createElement("h4");
        heading.innerHTML = obj.name;
        heading.classList.add("title");
        heroHead.appendChild(heading);
        if (CharacterInfo.level >= obj.levelAcquired) {
            hero.setAttribute("onclick", "showSkill(" + i + "," + false + ")");
        } else {
            var levelRequirement = document.createElement("h4");
            levelRequirement.innerHTML = "<i>Unlock at level " + obj.levelAcquired + "</i>";
            levelRequirement.classList.add("subtitle");
            heroHead.appendChild(levelRequirement);
        }
        var summary = document.createElement("p");
        summary.innerHTML = obj.summary;
        listEntry.appendChild(summary);
        document.getElementById("classAbilityList").appendChild(listEntry);
    });

    if ((CharacterInfo.subclass == null) && CharacterInfo.level >= ClassInfo.subclassLevel) {
        showSubclassSelector();
    }
    $("#ClassInfo").show();
}

function displayEquipment() {
    hideAllInfo();
    document.getElementById("displayEquipmentTab").classList.add("is-active");
    $("#EquipmentInfo").show();
}

function hideAllInfo() {
    $("#mainContent").children('div').each(function (i, obj) {
        $(obj).hide();
    });
    $("#navigationTab").children('li').each(function (i, obj) {
        obj.classList.remove("is-active");
    });
}

function showModal(value) {
    if (value)
        document.getElementById("pageNotification").classList.add("is-active");
    else
        document.getElementById("pageNotification").classList.remove("is-active");
}

function showSubclassSelector() {
    showModal(true);
}

function showSkill(index, isSubclass) {
    showModal(true);
    if (isSubclass) {

    } else {
        var content = document.getElementById("pageNotificationContent");
        $(content).empty();

        var obj = ClassInfo.abilities[index];

        var listEntry = document.createElement("box");
        listEntry.classList.add("box");
        var hero = document.createElement("div");
        hero.classList.add("hero");
        hero.classList.add("is-dark");
        listEntry.appendChild(hero);
        var heroHead = document.createElement("div");
        heroHead.classList.add("hero-head");
        hero.appendChild(heroHead);
        var heading = document.createElement("h4");
        heading.innerHTML = obj.name;
        heading.classList.add("title");
        heroHead.appendChild(heading);
        var summary = document.createElement("p");
        summary.innerHTML = obj.summary + "<br/>" + obj.detail;
        listEntry.appendChild(summary);

        content.appendChild(listEntry);
    }
}
